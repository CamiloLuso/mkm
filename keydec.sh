#!/bin/sh
# Configuration
set -eu

# Usage
echo "mkm:help> keydec # Input from stdin" >&2

# Commnad
gpg --decrypt
