#!/bin/sh
# Configuration
set -eu
CIPHER="AES256"
HASH="SHA512"

# Usage
echo "mkm:help> keyenc # Input from stdin" >&2

# Command
gpg --symmetric --cipher-algo "${CIPHER}" --digest-algo "${HASH}" --armor
