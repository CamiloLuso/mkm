#!/bin/sh
# Configuration
set -eu

# Usage
echo "mkm:help> keygen <length>" >&2

# XXX Arguments for options go after "--"
gpg --gen-random --armor -- 1 "${1}"
