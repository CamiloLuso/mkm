# MKM: Manual Key Manager

> Set of tools to generate, encrypt and decrypt keys based on GPG.

# Install

1. Install GPG.
2. Move to a preferred `$PATH`.

```
mv *.sh /bin/
```

# Usage

1. Generate a password once.

```
keygen 8
```

2. Encrypt that password.

```
echo "password" | keyenc
```

3. Decrypt that encrypted password.

```
echo "encrypted" | keydec
```

**Note:** you can use pipes or file redirection.
